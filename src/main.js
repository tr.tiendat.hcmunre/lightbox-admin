// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import './init';
import '@/common/helpers/validators/veeValidate';
import './mixins';
import '@/assets/libraries/adminlte/css';
import '@/assets/libraries/adminlte/script';

import Vue from 'vue';
import { createGuardedRouter } from './common/helpers/utils';
import Router from './router';
import App from './App.vue';
import store from '@/store';

Vue.config.productionTip = false;

const router = initRouter();

new Vue({
	store,
	el: '#app',
	router,
	render: h => h(App)
	// components: { App },
	// template: '<App/>'
});

function initRouter() {

	const router = createGuardedRouter(() => Router);

	router.beforeEach((to, from, next) => {
		if (!to.matched.length) {
			next({ name: 'not-found' });
			return;
		}

		next();
	});

	return router;
}
