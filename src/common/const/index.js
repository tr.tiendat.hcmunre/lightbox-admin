// import constants from './modules';

// export default constants;
const files = require.context('./modules', false, /\.js$/);
let constants = {};

files.keys().forEach((key) => {
	Object.assign(constants, files(key));
});

export default constants;
