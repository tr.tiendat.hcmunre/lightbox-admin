export const BASE_API_URL = process.env.VUE_APP_BASE_API;

export const API_URL = {
	// Guest constant
	LOGIN: 'signin',
	REGISTER: 'signup',
	SIGNOUT: 'signout',
	SIGNIN: 'admin-signin',

	// clients constant
	CLIENT: 'clients',

	AD_SETTING: 'attributes/default',
	AD_ATTRIBUTE: 'attributes',
	AGENCIES: 'agencies',

	// Current user constant
	PROFILE: 'me',
	AVATAR: 'avatar',

};

export const REQUEST_ACTION = {
	ADD: 'add',
	EDIT: 'edit',
	UPDATE: 'update',
	DELETE: 'delete'
};

export const REQUEST_TYPE = {
	GET: 'GET',
	POST: 'POST',
	PUT: 'PUT',
	DEL: 'DELETE',
};
