'use strict';

const env = process.env.NODE_ENV || 'development';

const isDebug = env === 'development' || env === 'testing' || env === 'staging';

isDebug && console.log("Start project with environment :", env);

class Config {
	constructor() {
		let config = process.env || {};

		this.config = config;
		this.isDebug = isDebug;
	}

	get(key) {
		return this.config[key] || '';
	}
};

export default new Config();
