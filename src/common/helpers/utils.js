import Vue from 'vue';
import events from '@/events';
import moment from 'moment';

export function normalizeDate (value) {
	return value ? moment.utc(value.date ? value.date : value) : null;
}

export function copyToClipboard (text) {
	const iframe = document.createElement('iframe');

	document.body.appendChild(iframe);

	const iframeWindow = iframe.contentWindow;
	const iframeDoc = iframeWindow.document;

	iframeWindow.designMode = true;

	const textReplaced = `${text}`.replace(/[\u00A0-\u9999<>&]/gim, i => `&#${i.charCodeAt(0)};`);
	iframeDoc.write(textReplaced);

	let range;
	if (iframeDoc.selection) {
		iframeDoc.selection.empty();
		range = iframeDoc.body.createTextRange();
		range.moveToElementText(iframeDoc.body);
		range.select();
	} else if (iframeWindow.getSelection()) {
		iframeWindow.getSelection().removeAllRanges();
		range = iframeDoc.createRange();
		range.selectNode(iframeDoc.body);
		iframeWindow.getSelection().addRange(range);
	}

	let result = false;
	try {
		if (iframeDoc.queryCommandEnabled('copy')) {
			iframeWindow.focus();
			result = iframeDoc.execCommand('copy');
		}
	} catch (e) {
		// ignore
	}

	document.body.removeChild(iframe);

	return result;
}

export function registerComponent (vm, name, component) {
	if (name in vm.$options.components) return; // don't overwrite components, strange behavior ensues

	vm.$options.components[name] = component;
}

export function pushOnBeforeUnload (msg) {
	const oldOnBeforeUnload = window.onbeforeunload;

	window.onbeforeunload = (e) => {
		e.returnValue = e;

		return msg;
	};

	let calledClear = false;
	return () => {
		if (calledClear) return;

		window.onbeforeunload = oldOnBeforeUnload;
		calledClear = true;
	};
}

let guardedRouterCreated = false;
export function createGuardedRouter (createRouter) { // shit is hacky AF. deal with it.
	if (guardedRouterCreated) {
		throw new Error('Only one guarded router can be created on a page');
	}
	guardedRouterCreated = true;

	let pushStack = (window.history.state && window.history.state._stack) || 0; // eslint-disable-line no-underscore-dangle
	let navSource = null;
	let ignoreNextPop = false;

	window.addEventListener('popstate', (e) => {
		const targetStack = (e.state && e.state._stack) || 0; // eslint-disable-line no-underscore-dangle
		navSource = targetStack > pushStack ? 'forward' : 'back';
		pushStack = targetStack;
	});

	const router = createRouter();

	const basePushState = window.history.pushState;
	window.history.pushState = (data, title, url) => {
		pushStack += 1;
		basePushState.call(window.history, { ...data, _stack: pushStack }, title, url); // eslint-disable-line no-underscore-dangle
	};

	const basePush = router.push;
	router.push = (...args) => {
		navSource = 'push';
		basePush.apply(router, args);
	};

	const baseReplace = router.replace;
	router.replace = (...args) => {
		navSource = 'replace';
		baseReplace.apply(router, args);
	};

	let navGuards = [];
	events.$on('guardNav', cb => navGuards.push(cb));
	events.$on('unguardNav', cb => (navGuards = navGuards.filter(g => g !== cb)));

	const baseConfirmTransition = router.history.confirmTransition;
	router.history.confirmTransition = (to, ...args) => {
		const source = navSource;

		if (ignoreNextPop && (source === 'back' || source === 'forward')) {
			ignoreNextPop = false;
			return;
		}

		const retryCallback = () => {
			Vue.nextTick(() => {
				switch (source) {
				default:
				case 'push':
				case 'replace':
					router[source](to.fullPath);
					break;
				case 'back':
				case 'forward':
					window.history[source]();
					break;
				}
			});
		};

		const current = router.history.current;
		const shouldPrevent = [...navGuards].reverse().some(g => g(to, current, retryCallback) === false);

		if (shouldPrevent) {
			if (source === 'back') {
				ignoreNextPop = true;
				window.history.forward();
			} else if (source === 'forward') {
				ignoreNextPop = true;
				window.history.back();
			}
		} else {
			baseConfirmTransition.apply(router.history, [to, ...args]);
		}

		navSource = null;
	};

	return router;
}
