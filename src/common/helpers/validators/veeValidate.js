import Vue from 'vue';
import { Validator, install as VeeValidate } from 'vee-validate';

Validator.extend('truthy', {
	getMessage: (field) => {
		return 'The ' + field + ' value is not truthy.';
	},
	validate: (value) => {
		return !!value;
	}
});

Validator.extend('less_than_eq', {
	getMessage: (field, args) => {
		return 'The ' + field + ' is must be less than or equal.';
	},
	validate: (value, {compareValue} = {}) => {
		let v1 = value,
			v2 = compareValue;
		if(typeof value === 'string') {
			let arr = value.split(':');
			v1 = parseInt(arr[0]) * 60 + parseInt(arr[1]);
		}
		if(typeof compareValue === 'string') {
			let arr = compareValue.split(':');
			v2 = parseInt(arr[0]) * 60 + parseInt(arr[1]);
		}
		return v1 <= parseInt(v2 || 0);
	}
}, {
	hasTarget: true,
	paramNames: ['compareValue']
});

Validator.extend('is_integer', {
	getMessage: (field) => {
		return 'The ' + field + ' is must be integer number.';
	},
	validate: (value) => {
		return Number.isInteger(Number(value));
	}
}, {
	hasTarget: true,
	paramNames: ['compareValue']
});

Vue.use(VeeValidate, {
	events: ''
});
