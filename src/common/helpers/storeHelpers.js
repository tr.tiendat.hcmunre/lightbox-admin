import Vuex, { mapActions, mapGetters } from 'vuex';

function deleteEmptyDefaultExport (component) {
	if ('default' in component && typeof component.default !== 'function') {
		delete component.default;
	}
}

export function commitError (commit, type = null) {
	return (res) => {
		commit(type ? `${type}Error` : 'error', res.error);
	};
}

export function defaultResetMutation (initialState) {
	return (state) => {
		Object.assign(state, initialState);
	};
}

export function defaultResetAction () {
	return ({ commit }) => commit('reset');
}

export function createStore (initialState, actions, mutations, getters) {
	if (!mutations.reset) {
		mutations.reset = defaultResetMutation(initialState);
	}

	if (!actions.reset) {
		actions.reset = defaultResetAction();
	}

	deleteEmptyDefaultExport(actions);
	deleteEmptyDefaultExport(mutations);
	deleteEmptyDefaultExport(getters);

	Object.keys(initialState).forEach((k) => {
		if (!(k in getters)) {
			getters[k] = state => state[k];
		}
	});

	return new Vuex.Store({ state: Object.assign({}, initialState), actions, mutations, getters });
}

export function vuexHelper (store) {
	return {
		store,

		methods: mapActions(Object.keys(store._actions)), // eslint-disable-line no-underscore-dangle
		computed: mapGetters(Object.getOwnPropertyNames(store.getters)),
	};
}
