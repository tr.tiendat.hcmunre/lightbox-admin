const get = (key = '') => {
	let value = localStorage.getItem(key);
	if(value) {
		return JSON.parse(value);
	}
	return value;
};

const set = (key = '', value = '') => {
	return localStorage.setItem(key, JSON.stringify(value));
};

const remove = (key = '') => {
	return localStorage.removeItem(key);
};

export default {
	get,
	set,
	remove
};
