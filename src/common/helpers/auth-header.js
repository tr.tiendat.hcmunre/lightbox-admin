export const authHeader = () => {
	// return authorization header with jwt token
	let token = JSON.parse(localStorage.getItem('token'));

	if (token) {
		return {
			'Authorization': `${token}`,
			// Cookie: `jwt=${token}`
		};
	}

	return {};
};

// export const authCookie = () =>{
// 	 Cookie: "cookie1=value; cookie2=value; cookie3=value;"
// }
