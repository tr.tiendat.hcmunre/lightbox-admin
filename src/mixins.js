import Vue from 'vue';

Vue.mixin({
	methods: {
		hasSlot(name = 'default') {
			return this.$slots[name] && this.$slots[name].length;
		},

		uid(id) {
			return `${this._uid}_${id}`; // eslint-disable-line no-underscore-dangle
		},

		noop() {
			// noop for stopping propagation
		},
	},

	created() {
		this.$main = this.$root.$children[0];
	},
});
