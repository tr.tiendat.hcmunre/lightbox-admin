import Vue from 'vue';
import Router from 'vue-router';
import About from '@/pages/About';
import Profile from '@/pages/Profile/Profile.vue';
import Home from '@/pages/Home/Home.vue';
import Main from "@/layout/Main.vue";
import Login from "@/pages/Login/Login.vue";
import { default as AgenciesEdition } from '@/pages/Agencies/Edition/Edition.vue';
import { default as AgenciesManagement } from '@/pages/Agencies/Management/Management.vue';
import AdSetting from '@/pages/AdSetting/AdSetting.vue';
import { default as LocationEdition } from '@/pages/AdSetting/Location/Edition/Edition.vue';
import { default as LocationManagement } from '@/pages/AdSetting/Location/Management/Management.vue';
import { default as NearbyEdition } from '@/pages/AdSetting/Nearby/Edition/Edition.vue';
import { default as NearbyManagement } from '@/pages/AdSetting/Nearby/Management/Management.vue';
import { default as TimeEdition } from '@/pages/AdSetting/Time/Edition/Edition.vue';
import { default as TimeManagement } from '@/pages/AdSetting/Time/Management/Management.vue';

Vue.use(Router);

export const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Main,
			children: [
				{
					path: '/dashboard',
					name: 'Dashboard',
					component: Home
				},
				{
					path: '/profile',
					name: 'Profile',
					component: Profile
				},
				{
					path: '/agencies/add',
					name: 'Agencies-Add',
					component: AgenciesEdition
				},
				{
					path: '/agencies/:id',
					name: 'Agencies-Edit',
					component: AgenciesEdition
				},
				{
					path: '/agencies',
					name: 'Agencies-Management',
					component: AgenciesManagement
				},
				{
					path: '/ad-setting',
					name: 'Ad-Setting',
					component: AdSetting
				},
				{
					path: '/locations',
					name: 'Location-Management',
					component: LocationManagement
				},
				{
					path: '/locations/add',
					name: 'Location-Add',
					component: LocationEdition
				},
				{
					path: '/locations/:id',
					name: 'Location-Edit',
					component: LocationEdition
				},
				{
					path: '/nearby',
					name: 'Nearby-Management',
					component: NearbyManagement
				},
				{
					path: '/nearby/add',
					name: 'Nearby-Add',
					component: NearbyEdition
				},
				{
					path: '/nearby/:id',
					name: 'Nearby-Edit',
					component: NearbyEdition
				},
				{
					path: '/times',
					name: 'Time-Management',
					component: TimeManagement
				},
				{
					path: '/times/add',
					name: 'Time-Add',
					component: TimeEdition
				},
				{
					path: '/times/:id',
					name: 'Time-Edit',
					component: TimeEdition
				},
			]
		},
		{
			path: '/about',
			name: 'About',
			component: About
		},
		{
			path: "/login",
			name: "Login",
			component: Login
		}
	],
	scrollBehavior: (to, from, savedPosition) => (savedPosition || { x: 0, y: 0 }),
});

export default router;
