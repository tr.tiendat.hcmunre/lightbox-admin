import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import events from './events';
import constants from './common/const';
import Notify from './common/helpers/notify';
import ErrorHandler from './common/helpers/api/errorHandler';

Vue.use(VueAxios, axios);

Vue.events = Vue.prototype.$events = events;
Vue.const = Vue.prototype.$const = constants;
Vue.notify = Vue.prototype.$notify = Notify;
Vue.errorHandler = Vue.prototype.$errorHandler = ErrorHandler;

axios.interceptors.request.use((request) => {
	const method = request.method.toUpperCase();
	if (method === 'PUT' || method === 'DELETE' || method === 'PATCH') {
		request.headers['X-HTTP-Method-Override'] = method;
		request.method = 'post';
	}
	return request;
});
