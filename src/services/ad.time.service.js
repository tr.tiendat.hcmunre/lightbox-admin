import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function loadTime(id) {
	return api.get(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function addTime(time) {
	return api.post(`${API_URL.AD_ATTRIBUTE}`, time, {useFormData: true});
};

export function updateTime(id, time) {
	return api.put(`${API_URL.AD_ATTRIBUTE}/${id}`, time, {useFormData: true});
};

export function removeTime(id) {
	return api.delete(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function loadTimes() {
	return api.get(`${API_URL.AD_ATTRIBUTE}`);
};
