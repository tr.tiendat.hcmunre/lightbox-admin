import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function loadAdSetting() {
	return api.get(`${API_URL.AD_SETTING}`);
};
