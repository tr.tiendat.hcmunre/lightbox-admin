import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function loadLocation(id) {
	return api.get(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function addLocation(location) {
	return api.post(`${API_URL.AD_ATTRIBUTE}`, location);
};

export function updateLocation(id, location) {
	return api.put(`${API_URL.AD_ATTRIBUTE}/${id}`, location);
};

export function removeLocation(id) {
	return api.delete(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function loadLocations() {
	return api.get(`${API_URL.AD_ATTRIBUTE}`);
};
