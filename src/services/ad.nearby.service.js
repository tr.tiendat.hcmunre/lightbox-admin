import { axios as api } from '../common/helpers/api';
import { API_URL } from '../common/configs/api';

export function loadNearby(id) {
	return api.get(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function addNearby(nearby) {
	return api.post(`${API_URL.AD_ATTRIBUTE}`, nearby, {useFormData: true});
};

export function updateNearby(id, nearby) {
	return api.put(`${API_URL.AD_ATTRIBUTE}/${id}`, nearby, {useFormData: true});
};

export function removeNearby(id) {
	return api.delete(`${API_URL.AD_ATTRIBUTE}/${id}`);
};

export function loadNearbys() {
	return api.get(`${API_URL.AD_ATTRIBUTE}`);
};
