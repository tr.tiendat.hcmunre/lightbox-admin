import { axios as api } from '../common/helpers/api';
import { API_URL, REQUEST_ACTION } from '../common/configs/api';

export function loadAgency(id) {
	return api.get(`${API_URL.AGENCIES}/${id}`);
};

export function addAgency(agency) {
	return api.post(`${API_URL.AGENCIES}/${REQUEST_ACTION.ADD}`, agency, {useFormData: true});
};

export function updateAgency(id, agency) {
	return api.put(`${API_URL.AGENCIES}/${id}`, agency);
};

export function removeAgency(id) {
	return api.delete(`${API_URL.AGENCIES}/${id}`);
};

export function loadAgencies() {
	return api.get(`${API_URL.AGENCIES}`);
};
