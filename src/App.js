"use strict";

import { mapActions, mapGetters } from 'vuex';
import { GETTERS, MUTATES, ACTIONS } from './store';
export default {
	// constructor() {
	// 	if (!this.isAuthenticated) {
	// 		this.loadMe();
	// 	}
	// },

	name: "App",
	data() {
		return {};
	},
	computed: {
		...mapGetters([GETTERS.LOADING, GETTERS.IS_AUTHENTICATED, GETTERS.CURRENT_USER])
	},
	async created() {
		if (!this.isAuthenticated) {
			this.$store.commit(MUTATES.LOADING);
			await this.loadMe().then(response => {
			});
			this.$store.commit(MUTATES.LOADED);
		} else {
		}
	},
	methods: {
		...mapActions([ACTIONS.LOAD_ME])
	}
};
