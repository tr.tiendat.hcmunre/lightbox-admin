export default {
	data: () => ({
		skins: [
			'skin-blue',
			'skin-black',
			'skin-red',
			'skin-yellow',
			'skin-purple',
			'skin-green',
			'skin-blue-light',
			'skin-black-light',
			'skin-red-light',
			'skin-yellow-light',
			'skin-purple-light',
			'skin-green-light'
		]
	}),
	name: 'ControlSideBar',
	components: {}
};
