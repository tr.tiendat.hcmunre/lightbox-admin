import { mapGetters, mapActions } from "vuex";
import ContentWrap from "./ContentWrap/ContentWrap.vue";
import SideBar from "./SideBar/SideBar.vue";
import NavBar from "./NavBar/NavBar.vue";
import ControlSideBar from "./ControlSideBar/ControlSideBar.vue";
import menuItems from "./menuItems";
import config from "@/common/configs/env";

export default {
	name: "Main",
	data() {
		return {
			menuItems: menuItems
			// skin: this.$store.getters.skin
		};
	},
	computed: {
		...mapGetters(["skin", "currentUser"])
	},
	components: {
		"content-wrap": ContentWrap,
		"nav-bar": NavBar,
		sidebar: SideBar,
		"control-sidebar": ControlSideBar
	},
	watch: {
		skin(val) {
			this.updateUI();
			this.skin = val;
		}
	},
	mounted() {
		if (!this.currentUser._id) {
			return this.$router.push("Login");
		}

		$('.sidebar-menu').each(function () {
			$.fn.tree.call($(this));
		});

		this.$events.$on(this.$const.SET_TITLE, title => {
			document.title = title
				? `${title} • ${config.get("title")}`
				: config.get("title");
		});

		this.$events.$emit(this.$const.SET_TITLE, "Dashboard");

		this.$nextTick(() => {
			this.updateUI();
		});
	},
	updated() {
		console.log("app  updated");
	},
	methods: {
		...mapActions([
			// Mount action "setNumberToRemoteValue" thành `this.setNumberToRemoteValue()`.
			"setSkin"
		]),
		updateUI() {
			$("#foo").alterClass("skin-* bar-*", this.skin);
			// document.body.classList;
		}
	}
};
