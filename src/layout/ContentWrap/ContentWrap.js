export default {
	name: 'ContentWrap',
	// data: () => ({
	//     title: 'Dashboard'
	// }),
	data() {
		return {
			title: 'Dashboard',
			breadcrumbs: ['Dashboard']
		};
	},
	created() {
		this.$events.$on(this.$const.SET_PAGE_TITLE, (title) => {
			this.title = title || 'Dashboard';
		});

		this.$events.$on(this.$const.SET_BREADCRUMB, (breadcrumbs) => {
			this.breadcrumbs = breadcrumbs || [];
		});
	},
	mounted() {
		// console.log('loaded ContentWrap');
		// this.$events.$on('setComTitle', (title) => {
		//     console.log("setComTitle", title);
		//     this.title = title || 'Dashboard';
		// });
		$(this.$el).trigger("resize");
	}
};
