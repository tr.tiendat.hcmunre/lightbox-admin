export default [
	{
		type: "item",
		isHeader: true,
		name: "MAIN NAVIGATION"
	},
	{
		icon: "fa fa-dashboard",
		name: "Dashboard",
	},
	{
		type: "tree",
		icon: "fa fa-users",
		name: "Client managament",
		items: [
			{
				type: "item",
				icon: "fa fa-list-alt",
				name: "List",
				router: {
					name: "Clients-Management"
				}
			},
		],
	},
	{
		type: "tree",
		icon: "fa fa-users",
		name: "Agencies Managament",
		items: [
			{
				type: "item",
				icon: "fa fa-list-alt",
				name: "List",
				router: {
					name: "Agencies-Management"
				}
			}
		]
	},
	{
		type: "tree",
		icon: "fa fa-users",
		name: "Ads Attribute Settings",
		items: [
			{
				type: "item",
				icon: "fa fa-list-alt",
				name: "List",
				router: {
					name: "Ad-Setting"
				}
			},
			{
				type: "item",
				icon: "fa fa-list-alt",
				name: "Location",
				router: {
					name: "Location-Management"
				}
			},
			// {
			// 	type: "item",
			// 	icon: "fa fa-list-alt",
			// 	name: "Nearby",
			// 	router: {
			// 		name: "Nearby-Management"
			// 	}
			// },
			// {
			// 	type: "item",
			// 	icon: "fa fa-list-alt",
			// 	name: "Time",
			// 	router: {
			// 		name: "Time-Management"
			// 	}
			// }
		]
	},
];
