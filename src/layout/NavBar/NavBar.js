import { mapGetters, mapActions } from 'vuex';
import { GETTERS, ACTIONS } from '@/store';

// import SideBar from './SideBar.vue';

export default {
	name: 'SideBar',
	props: {
		menuItems: {
			type: Array,
			default: () => []
		}
	},
	data() {
		return {
			isVisible: false,
			remainningPoints: 0,
			usagePoints: 0
		};
	},
	created() { },
	computed: {
		...mapGetters([
			GETTERS.UNREAD_MSG_COUNT,
			GETTERS.UNREAD_NOTICE_COUNT,
			GETTERS.CURRENT_USER,
			GETTERS.AVATAR
		])
	},
	mounted() {
		this.setDataUser();
	},
	methods: {
		...mapActions([ACTIONS.SIGNOUT]),
		logout() {
			this.signout().then(res => {
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}

				this.$router.push({ name: 'Login' });
			});
		},
		routeToProfile() {
			this.$router.push({ name: 'Profile' });
		},
		getAvatar() {
			return this.avatar;
		},
		setDataUser() {
			this.remainningPoints = this.currentUser.balance;
			this.user = this.currentUser.name;
		}
	}
};
