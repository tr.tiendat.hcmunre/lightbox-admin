
import { mapGetters } from 'vuex';
import SlideItem from 'components/SlideItem/SlideItem.vue';
import { GETTERS } from '@/store';

export default {
	name: 'SideBar',
	props: {
		menuItems: {
			type: Array,
			default: []
		}
	},
	created () {},
	computed: {
		...mapGetters([
			GETTERS.CURRENT_USER,
			GETTERS.AVATAR
		])
	},
	components: {
		'slide-item': SlideItem,
	},
	methods: {
		getAvatar() {
			return this.avatar;
		}
	},
};
