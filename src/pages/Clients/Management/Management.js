"use strict";
import { mapGetters, mapActions } from 'vuex';
import User from '../../Profile/User/User.vue';
import Config from '../../../common/configs/env';
import { GETTERS, ACTIONS } from '../../../store/const';

export default {
	data: () => ({
		loaded: true,
		showConfirm: false,
		clientDelete: null
	}),
	components: {
		'user-info': User,
	},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	mixins: [
		// vuexHelper(store),
	],

	methods: {
		...mapActions({
			removeClient: 'client/remove'
		}),
		...mapActions([ACTIONS.LOAD]),
		reload() {
			this.load();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		edit(id) {
			this.$router.push({ name: 'Clients-Edit', params: { id: id } });
		},
		confirmRemove(client) {
			this.clientDelete = client;
			$("#modal-confirm").modal('show');
		},
		remove() {
			if(!this.clientDelete) { return }

			this.removeClient(this.clientDelete._id).then(res => {
				// this.$error
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.table.ajax.reload(null, false); // user paging is not reset on reload
				this.clientDelete = null;
				$("#modal-confirm").modal('hide');
				this.$notify.success(`Remove client successfully`);
			});
		},
		cancel() {
			this.showConfirm = false;
			this.clientDelete = null;
		}
	},
	mounted() {
		var that = this;
		this.reload();
		var table = this.table = $(this.$el).find('#clients').DataTable({
			"serverSide": true,
			"processing": true,
			"pageLength": 10,
			"ajax": $.fn.dataTable.pipeline({
				url: `${Config.get('apiUrl')}/api/v1/clients`
			}),
			// 'paging': true,
			// 'lengthChange': true,
			// 'searching': true,
			// 'ordering': true,
			'info': true,
			'autoWidth': false,
			"columns": [
				{ "data": "name", "width": "20%" },
				{ "data": "email" },
				{ "data": "phone" },
				{ "data": "address", "width": "20%" },
				{ "data": "balance", "width": "80px" },
				{ "data": "status" },
				{
					"data": null,
					"width": "70px",
					"defaultContent": '<a class="btn btn-info edit"><i class="fa fa-pencil"></i></a> ' +
									'<a  class="btn btn-danger delete"><i class="fa fa-trash"></i></a>'
				}
			],
			"language": {
				"search": "",
				"searchPlaceholder": "Filter",
				// "info": "filter results: _END_  total: _TOTAL_",
				// "infoEmpty": "filter result: 0 total: 0",
			},
			"dom": '<"pull-left"f><"col-md-6"iB><"fix-table"t><"clear"><"col-xs-6 col-md-6"l><"col-xs-6 col-md-6"p>'
		});

		$('#clients tbody').on('click', 'a', function () {
			var data = table.row($(this).parents('tr')).data();
			if ($(this).hasClass('edit')) {
				that.edit(data._id);
			} else if ($(this).hasClass('delete')) {
				that.confirmRemove(data);
			}
		});
		this.table = table;
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.CLIENTS_MANAGEMENT);
	},
};
