"use strict";
import { mapActions } from 'vuex';

const enumActive = {
	INACTIVE: 'inactive',
	ACTIVE: 'active'
};

export default {
	data: () => ({
		loading: false,
		options: [
			{ id: 'inactive', text: 'Inactive' },
			{ id: 'active', text: 'Active' }
		],
		formData: {
			name: null,
			status: enumActive.INACTIVE,
			email: null,
			phone: null,
			address: null
		}
	}),
	components: {},
	watch: {
		$route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		},
	},
	updated() {
		let that = this;
		$(this.$el).find('.select2')
			.select2({ data: this.options, minimumResultsForSearch: Infinity })
			.val(this.formData.status)
			.one('change', function () {
				that.formData.status = this.value;
			});
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$route.params.id ? this.$const.CLIENTS_EDITION : this.$const.CLIENTS_CREATION);
	},
	created() {
		this.reload();
		const dict = {
			custom: {
				name: {
					required: 'Name is required and must not start nor finish by spacing characters.',
					regex: 'Name must not start nor finish by spacing characters.'
				},
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {},

	methods: {
		...mapActions({
			loadClient: 'client/load',
			editClient: 'client/editClient'
		}),
		reset() {
			this.formData = {
				name: null,
				status: enumActive.INACTIVE,
				email: null,
				phone: null,
				address: null
			};
		},
		reload() {
			this.reset();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadClient(this.$route.params.id).then(res => {
					this.formData = res.client;
					this.loading = false;
				});
			}
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			this.$validator.validateAll().then((result) => {
				if (result) {
					this.editClient(this.formData).then(res => {
						if(res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}

						this.$notify.success('Save client successfuly');
						return this.$router.push({ name: "Clients-Management" });
					});
				}
			});
		},
	}
};
