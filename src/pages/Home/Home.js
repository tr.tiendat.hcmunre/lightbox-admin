"use strict";
import { mapGetters, mapActions } from 'vuex';
import { GETTERS, ACTIONS } from '../../store/const';
// import store from '@/vuex/home';
// import { vuexHelper } from '@/vuex/_helpers';

export default {
	data: () => ({
		loaded: true
	}),
	components: {},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		}
	},

	mixins: [
		// vuexHelper(store),
	],

	mounted() {
		this.reload();
	},

	methods: {
		...mapActions([
			ACTIONS.LOAD
		]),
		reload() {
			this.load();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		}
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.DASHBOARD);
	}
};
