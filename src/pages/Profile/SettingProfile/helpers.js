import axios from 'axios';
import { Validator } from 'vee-validate';

const API_CHECK_NAME_UNIQUE = '/api/validate/name';

const isUnique = (value) => {
    return axios.post(API_CHECK_NAME_UNIQUE, { email: value }).then((response) => {
        return {
            valid: response.data.valid,
            data: {
                message: response.data.message
            }
        };
    });
};

Validator.extend('unique', {
    validate: isUnique,
    getMessage: (field, params, data) => {
        return data.message;
    }
});

export const submitForm = (formData = {}) => {
    axios.post(
        API_CHECK_NAME_UNIQUE,
        formData
    ).then((response) => {
        return {
            valid: response.data.valid,
            data: {
                message: response.data.message
            }
        };
    });
};
