"use strict";

import { mapGetters } from "vuex";
import { GETTERS } from "../../../store";

export default {
	data: () => ({
		user: 'LB Agency',
		desJob: 'Software Engineer',
		remainningPoints: 0,
		usagePoints: 0
	}),
	computed: {
		...mapGetters([
			GETTERS.AVATAR,
			GETTERS.CURRENT_USER
		])
	},
	components: {

	},
	mounted() {
		this.setDataUser();
	},
	watch: {

	},
	methods: {
		getAvatar() {
			// return `http://localhost:8041/api/v1/avatar?_=${Date.now()}`;
			return this.avatar;
		},
		setDataUser() {
			this.remainningPoints = this.currentUser.balance;
			this.user = this.currentUser.name;
		}
	},
};
