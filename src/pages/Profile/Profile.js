"use strict";
import { mapGetters, mapActions } from 'vuex';
import SettingProfile from './SettingProfile/SettingProfile.vue';
import User from './User/User.vue';
import { GETTERS, ACTIONS } from '../../store/const';

export default {
	data: () => ({
		loaded: true
	}),
	components: {
		'user-info': User,
		'setting-profile': SettingProfile
	},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		}
	},

	mixins: [
		// vuexHelper(store),
	],

	mounted() {
		this.reload();
	},

	methods: {
		...mapActions([
			ACTIONS.LOAD
		]),
		reload() {
			this.load();
		}
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.PROFILE);
	}
};
