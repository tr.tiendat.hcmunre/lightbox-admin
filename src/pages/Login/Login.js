"use strict";

import { mapActions, mapGetters } from 'vuex';
import { GETTERS, ACTIONS } from '../../store';

export default {
	name: 'Login',
	data: () => ({
		form: {
			email: '',
			password: '',
			remember_me: false
		},
		saving: false
	}),
	computed: {
		...mapGetters([GETTERS.IS_AUTHENTICATED])
	},
	created() {
		const dict = {
			custom: {
				password: {
					required: "Signing in failed, please check your email and password.",
					regex: "Password should not start or end with spacing character(s)"
				},
				email: {
					required: "Signing in failed, please check your email and password",
					email: "Signing in failed, please check your email and password."
				}
			}
		};

		this.$validator.localize('en', dict);
	},
	updated() {
		if (this.isAuthenticated) {
			this.$router.push('Dashboard');
		}
		this.updateUI();
	},
	mounted() {
		if (this.isAuthenticated) {
			return this.$router.push('Dashboard');
		}

		if(this.$route.query.verify) {
			this.$notify.success(`Your email address ${this.$route.query.verify} has been verified successfully and your account has been activated. You can now sign in to your account.`, {
				position: 'bottom-center'
			});
		}
	},
	methods: {
		...mapActions([ACTIONS.SIGNIN]),
		updateUI() {
			let that = this;
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			}).one('ifChecked', (event) => {
				that.form.remember_me = event.target.checked;
			});
			$("body").addClass('hold-transition login-page');
		},
		showErrorClass(prop) {
			return this.errors.has(prop) ? 'has-error' : '';
		},
		submit() {
			this.$validator.validateAll().then(isValid => {
				if (isValid) {
					this.saving = true;

					this.signin(this.form).then(res => {
						this.saving = false;
						if (res.errors) {
							return this.$errorHandler.showServerErrors(res.errors, this);
						}
						return this.$router.push({name: 'Dashboard'});
					});
				}
			});
		}
	}
};
