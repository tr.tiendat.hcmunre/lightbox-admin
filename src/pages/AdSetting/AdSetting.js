"use strict";
import {mapGetters, mapActions} from 'vuex';
import {GETTERS, ACTIONS} from '@/store/const';
import {ACTIONS as AD_SETTING_ACTIONS} from "@/store/modules/ad/const";
import {ENUM_AD_SETTING} from "@/common/const/modules/ad.setting.const";

export default {
	data: () => ({
		loaded: true,
		adAttributes: []
	}),
	components: {},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	methods: {
		...mapActions({
			appLoad: ACTIONS.LOAD,
			loadAdSetting: AD_SETTING_ACTIONS.LOAD_ALL
		}),
		reload() {
			this.appLoad();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		async getAdAttribues() {
			return this.loadAdSetting().then(res => {
				return res.attributes || [];
			}).then(attrs => {
				return attrs;
			}).catch(err => {
				console.log('Errors load ad attributes settings: ', err);
				return false;
			});
		},
		setAdAttributes(data) {
			this.adAttributes = data;
		},
		async setDataAdAttributes() {
			const adAttrs = await this.getAdAttribues();
			this.setAdAttributes(adAttrs);
		},
		setMarkupCustomization(type) {
			let routerName = '';
			switch (type) {
			case ENUM_AD_SETTING.LOCATION:
				routerName = 'Location-Management';
				break;

			case ENUM_AD_SETTING.NEARBY:
				routerName = 'Nearby-Management';
				break;

			case ENUM_AD_SETTING.TIME:
				routerName = 'Time-Management';
				break;
			}

			return routerName;
		},
	},
	mounted() {
		this.setDataAdAttributes();
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.AD_SETTING_MANAGEMENT);
	},
};
