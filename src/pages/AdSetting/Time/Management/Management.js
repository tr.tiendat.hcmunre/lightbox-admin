"use strict";
import {mapGetters, mapActions} from 'vuex';
import {BASE_API_URL} from '@/common/configs/api';
import {GETTERS, ACTIONS} from '@/store/const';
import {ACTIONS as TIME_ACTIONS} from '@/store/modules/ad/time/const';

export default {
	data: () => ({
		loaded: true,
		showConfirm: false,
		timeNeedDeleted: null,
		deleteMsg: ''
	}),
	components: {},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	methods: {
		...mapActions({
			loadTime: TIME_ACTIONS.LOAD,
			removeTime: TIME_ACTIONS.REMOVE
		}),
		...mapActions({
			appLoad: ACTIONS.LOAD
		}),
		reload() {
			this.appLoad();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		edit(id) {
			this.$router.push({name: 'Time-Edit', params: {id: id}});
		},
		confirmRemove(time) {
			this.deleteMsg = `Are you sure to delete ${time.name}?`;
			this.timeNeedDeleted = time;
			$("#modal-confirm").modal('show');
		},
		remove() {
			if (!this.timeNeedDeleted) {
				return;
			}

			this.removeTime(this.timeNeedDeleted._id).then(res => {
				// this.$error
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.table.ajax.reload(null, false); // user paging is not reset on reload
				this.timeNeedDeleted = null;
				$("#modal-confirm").modal('hide');
				this.$notify.success(`Remove time successfully`);
			});
		},
		cancel() {
			this.showConfirm = false;
			this.timeNeedDeleted = null;
		}
	},
	mounted() {
		var that = this;
		this.reload();
		var table = this.table = $(this.$el).find('#times').DataTable({
			"serverSide": true,
			"processing": true,
			"pageLength": 10,
			"ajax": $.fn.dataTable.pipeline({
				url: `${BASE_API_URL}/times`
			}),
			'info': true,
			'autoWidth': false,
			"columns": [
				{
					"data": 'client_id',
					"render": function (data, type, row) {
						return (data || {name: ''}).name;
					}
				},
				{"data": "name", "width": "20%"},
				{"data": "file.file_type", "width": "20%"},
				{
					"data": "status",
					"render": function (data, type, row) {
						return data.charAt(0).toUpperCase() + data.slice(1);
					}
				},
				{"data": "balance", "width": "80px"},
				{"data": "balance", "width": "80px"},
				{
					"data": null,
					"width": "70px",
					"defaultContent": '<a class="btn btn-info edit"><i class="fa fa-pencil"></i></a> ' +
						'<a  class="btn btn-danger delete"><i class="fa fa-trash"></i></a>'
				}
			],
			"language": {
				"search": "",
				"searchPlaceholder": "Filter",
			},
			"dom": '<"pull-left"f><"col-md-6"iB><"fix-table"t><"clear"><"col-xs-6 col-md-6"l><"col-xs-6 col-md-6"p>'
		});

		$(this.el).find('#times tbody').on('click', 'a', function () {
			var data = table.row($(this).parents('tr')).data();
			if ($(this).hasClass('edit')) {
				that.edit(data._id);
			} else if ($(this).hasClass('delete')) {
				that.confirmRemove(data);
			}
		});
		this.table = table;
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.TIME_MANAGEMENT);
	},
};
