"use strict";
import {mapActions, mapGetters} from 'vuex';
import {ACTIONS as TIME_ACTIONS} from '@/store/modules/ad/time/const';
import {GETTERS} from "@/store";

export default {
	data: () => ({
		loading: false,
		formData: {
			_id: null,
			times: {
				from: null,
				to: null
			},
		},
		isSubmitting: false,
		remainingPoint: 0,
	}),
	components: {},
	computed: {
		...mapGetters({
			appLoad: GETTERS.LOADING,
		})
	},
	watch: {
		async $route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		}
	},
	updated() {
		console.log('updated fire');
		const that = this;
		// Time picker ==========
		// from time =======
		let from = this.formData.times.from || 0;
		if($(this.$el).find('.timepicker.from').data('timepicker')) {
			$(this.$el).find('.timepicker.from')
				.timepicker('setTime', from ? `${parseInt(from / 60)}:${from % 60}` : 'current');
		}else {
			$(this.$el).find('.timepicker.from').timepicker({
				showInputs: false,
				showMeridian: false,
				minuteStep: 5,
				// defaultTime: from ? `${parseInt(from / 60)}:${from % 60}` : 'current'
			}).addClass('has-init')
				.on('changeTime.timepicker', function (e) {
					that.formData.times.from = e.time.hours * 60 + e.time.minutes;
				});
		}

		// to time =======
		let to = this.formData.times.to || 0;
		if($(this.$el).find('.timepicker.to').data('timepicker')) {
			$(this.$el).find('.timepicker.to')
				.timepicker('setTime', to ? `${parseInt(to / 60)}:${to % 60}` : 'current');
		}else {
			$(this.$el).find('.timepicker.to').timepicker({
				showInputs: false,
				showMeridian: false,
				minuteStep: 5,
				defaultTime: to ? `${parseInt(to / 60)}:${to % 60}` : 'current'
			}).on('changeTime.timepicker', function (e) {
				that.formData.times.to = e.time.hours * 60 + e.time.minutes;
			});
		}

		this.$events.$emit(
			this.$const.SET_PAGE_TITLE,
			this.$const.TIME_MANAGEMENT);
	},
	async created() {
		this.reload();
		const dict = {
			custom: {
				name: {
					required: 'Name is required and must not start nor finish by spacing characters.',
					regex: 'Name must not start nor finish by spacing characters.'
				}
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},

	methods: {
		...mapActions({
			loadTime: TIME_ACTIONS.LOAD,
			editTime: TIME_ACTIONS.EDIT,
		}),
		clearErrorField(field) {
			this.errors.remove(field);
		},
		resetFields() {
			this.formData = {
				_id: null,
				times: {
					from: null,
					to: null
				},
			};
		},
		resetForm() {
			this.errors.clear();
			this.resetFields();
		},
		reload() {
			this.resetFields();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadTime(this.$route.params.id).then(res => {
					this.formData = res.time;
					this.loading = false;
				}).catch(err => {
					console.log('Errors load time: ', err);
					this.loading = false;
					return false;
				});
			}
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			if (!this.isSubmitting) {
				this.$validator.validateAll().then((result) => {
					if (result) {
						this.editLocation(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}
							this.isSubmitting = true;
							this.$notify.success('Save time successfuly');
							return this.$router.push({name: "Time-Management"});
						});
					}
				});
			}
		}
	}
};
