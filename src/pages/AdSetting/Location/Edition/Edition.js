"use strict";
import {mapActions, mapGetters} from 'vuex';
import {ACTIONS as LOCATION_ACTIONS} from '@/store/modules/ad/location/const';
import {GETTERS} from "@/store";
import SelectLocation from '../../../../components/SelectLocation/SelectLocation.vue';

export default {
	data: () => ({
		loading: false,
		formData: {
			type: 'location',
			_id: null,
			values: null,
			points: 0
		},
		location: {
			selected: '',
			data: {}
		},
		isSubmitting: false,
		remainingPoint: 0,
		selected: 2,
		locationOptions: [],
	}),
	components: {
		SelectLocation,
	},
	computed: {
		...mapGetters({
			appLoad: GETTERS.LOADING,
		})
	},
	watch: {
		async $route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		}
	},
	updated() {
		this.$events.$emit(
			this.$const.SET_PAGE_TITLE,
			this.$const.LOCATION_MANAGEMENT);
	},
	async created() {
		this.reload();
		const dict = {
			custom: {
				name: {
					required: 'Name is required and must not start nor finish by spacing characters.',
					regex: 'Name must not start nor finish by spacing characters.'
				}
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},

	methods: {
		...mapActions({
			loadLocation: LOCATION_ACTIONS.LOAD,
			editLocation: LOCATION_ACTIONS.EDIT,
		}),
		clearErrorField(field) {
			this.errors.remove(field);
		},
		resetFields() {
			this.formData = {
				type: 'location',
				_id: null,
				values: null,
				points: 0
			};
		},
		resetForm() {
			this.errors.clear();
			this.resetFields();
		},
		reload() {
			this.resetFields();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadLocation(this.$route.params.id).then(res => {
					this.formData = res.location;
					if(res.location.values && res.location.values.properties) {
						this.location = {
							selected: res.location.values.properties.place_id,
							data: res.location.values
						};
					}
					this.loading = false;
				}).catch(err => {
					console.log('Errors load location: ', err);
					this.loading = false;
					return false;
				});
			}
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			if (!this.isSubmitting) {
				this.$validator.validateAll().then((result) => {
					if (result) {
						this.isSubmitting = true;
						this.formData.name = this.formData.values.properties.display_name;
						this.editLocation(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}
							this.isSubmitting = false;
							this.$notify.success('Save location successfully');
							return this.$router.push({name: "Location-Management"});
						});
					}
				});
			}
		},
		onChangeLocation(evt) {
			this.formData.values = evt.data
			this.location.data = evt.data;
			this.location.selected = evt.value;
		}
	}
};
