"use strict";
import {mapGetters, mapActions} from 'vuex';
import {BASE_API_URL} from '@/common/configs/api';
import {GETTERS, ACTIONS} from '@/store/const';
import {ACTIONS as LOCATION_ACTIONS} from '@/store/modules/ad/location/const';

export default {
	data: () => ({
		loaded: true,
		showConfirm: false,
		locationNeedDeleted: null,
		deleteMsg: ''
	}),
	components: {},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	methods: {
		...mapActions({
			removeLocation: LOCATION_ACTIONS.REMOVE
		}),
		...mapActions({
			appLoad: ACTIONS.LOAD
		}),
		reload() {
			this.appLoad();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		edit(id) {
			this.$router.push({name: 'Location-Edit', params: {id: id}});
		},
		confirmRemove(location) {
			this.deleteMsg = `Are you sure to delete "${location.name}"?`;
			this.locationNeedDeleted = location;
			$("#modal-confirm").modal('show');
		},
		remove() {
			if (!this.locationNeedDeleted) {
				return;
			}

			this.removeLocation(this.locationNeedDeleted._id).then(res => {
				// this.$error
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.table.ajax.reload(null, false); // user paging is not reset on reload
				this.locationNeedDeleted = null;
				$("#modal-confirm").modal('hide');
				this.$notify.success(`Remove location successfully`);
			});
		},
		cancel() {
			this.showConfirm = false;
			this.locationNeedDeleted = null;
		}
	},
	mounted() {
		var that = this;
		this.reload();
		var table = this.table = $(this.$el).find('#locations').DataTable({
			"serverSide": true,
			"processing": true,
			"pageLength": 10,
			"ajax": $.fn.dataTable.pipeline({
				url: `${BASE_API_URL}/attributes/list/location`
			}),
			'info': true,
			'autoWidth': false,
			"columns": [
				{
					"data": 'name',
					"render": function (data, type, row) {
						return data || row.values.properties.display_name;
					}
				},
				{"data": "points"},
				{
					"data": null,
					"width": "70px",
					"defaultContent": '<a class="btn btn-info edit"><i class="fa fa-pencil"></i></a> ' +
						'<a  class="btn btn-danger delete"><i class="fa fa-trash"></i></a>'
				}
			],
			"language": {
				"search": "",
				"searchPlaceholder": "Filter",
			},
			"dom": '<"pull-left"f><"col-md-6"iB><"fix-table"t><"clear"><"col-xs-6 col-md-6"l><"col-xs-6 col-md-6"p>'
		});

		table.on('click', 'tbody a', function () {
			var data = table.row($(this).parents('tr')).data();
			if ($(this).hasClass('edit')) {
				that.edit(data._id);
			} else if ($(this).hasClass('delete')) {
				that.confirmRemove(data);
			}
		});
		this.table = table;
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.LOCATION_MANAGEMENT);
	},
};
