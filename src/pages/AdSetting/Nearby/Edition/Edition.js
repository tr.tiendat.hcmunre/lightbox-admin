"use strict";
import {mapActions, mapGetters} from 'vuex';
import {ACTIONS as NEARBY_ACTIONS} from '@/store/modules/ad/nearby/const';
import {GETTERS} from "@/store";

export default {
	data: () => ({
		loading: false,
		formData: {
			_id: null,
			nearby: null,
		},
		isSubmitting: false,
		remainingPoint: 0,
		nearByOptions: [
			{
				id: 'school',
				text: 'School'
			},
			{
				id: 'hotel',
				text: 'Hotel'
			},
			{
				id: 'hospital',
				text: 'Hospital'
			}
		],
	}),
	components: {},
	computed: {
		...mapGetters({
			appLoad: GETTERS.LOADING,
		})
	},
	watch: {
		async $route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		}
	},
	updated() {
		const that = this;

		if ($(this.$el).find("#nearby").hasClass("select2-hidden-accessible")) {
			$(this.$el).find("#nearby")
				.select2('destroy')
				.off('select2:select');
		}
		$(this.$el).find("#nearby")
			.select2({
				data: this.nearByOptions,
				minimumResultsForSearch: Infinity,
				placeholder: "Select a state",
				allowClear: true
			})
			.val((this.formData || {}).nearby)
			.on('select2:select', function (evt) {
				that.formData.nearby = $(this).val();
				evt.stopPropagation();
				evt.preventDefault();
			});

		this.$events.$emit(
			this.$const.SET_PAGE_TITLE,
			this.$const.NEARBY_MANAGEMENT);
	},
	async created() {
		this.reload();
		const dict = {
			custom: {
				name: {
					required: 'Name is required and must not start nor finish by spacing characters.',
					regex: 'Name must not start nor finish by spacing characters.'
				}
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},

	methods: {
		...mapActions({
			loadNearby: NEARBY_ACTIONS.LOAD,
			editNearby: NEARBY_ACTIONS.EDIT,
		}),
		clearErrorField(field) {
			this.errors.remove(field);
		},
		resetFields() {
			console.log('resetFields');
			this.formData = {
				_id: null,
				nearby: null,
			};
		},
		resetForm() {
			this.errors.clear();
			this.resetFields();
		},
		reload() {
			this.resetFields();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadNearby(this.$route.params.id).then(res => {
					this.formData = res.nearby;
					this.loading = false;
				}).catch(err => {
					console.log('Errors load nearby: ', err);
					this.loading = false;
					return false;
				});
			}
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			if (!this.isSubmitting) {
				this.$validator.validateAll().then((result) => {
					if (result) {
						this.editNearby(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}
							this.isSubmitting = true;
							this.$notify.success('Save nearby successfuly');
							return this.$router.push({name: "Nearby-Management"});
						});
					}
				});
			}
		}
	}
};
