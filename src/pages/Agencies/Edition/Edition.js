"use strict";
import {mapActions, mapGetters} from 'vuex';
import {ACTIONS as AGENCY_ACTIONS} from '@/store/modules/agency/const';
import {GETTERS} from "@/store";

const enumActive = {
	INACTIVE: 'inactive',
	ACTIVE: 'active'
};

export default {
	data: () => ({
		loading: false,
		options: [
			{id: 'inactive', text: 'Inactive'},
			{id: 'active', text: 'Active'}
		],
		formData: {
			_id: null,
			name: null,
			email: null,
			phone: null,
			password: null,
			confirmPassword: null,
			address: null,
			logo: {
				file: null,
				info: {
					size: null,
					type: null,
				}
			},
			status: enumActive.INACTIVE
		},
		remainingPoint: 0,
		usagePoint: 0,
		isSubmitting: false,
		isNeedAddMorePoint: false
	}),
	components: {},
	computed: {
		...mapGetters({
			appLoad: GETTERS.LOADING,
			currentUser: GETTERS.CURRENT_USER
		})
	},
	watch: {
		async $route(to, from) {
			if (to.path !== from.path) {
				this.reload();
			}
		},
		loading(val) {
			this.loaded = !val;
		},
	},
	updated() {
		let that = this;
		$(this.$el).find('.status.select2')
			.select2({data: this.options, minimumResultsForSearch: Infinity})
			.val(this.formData.status)
			.one('change', function () {
				that.formData.status = this.value;
			});

		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$route.params.id ? this.$const.AGENCY_EDITION : this.$const.AGENCY_CREATION);
	},
	async created() {
		this.reload();
		const dict = {
			custom: {
				name: {
					required: 'Name is required.'
				},
			}
		};

		this.$validator.localize('en', dict);
	},
	mounted() {
	},

	methods: {
		...mapActions({
			loadAgency: AGENCY_ACTIONS.LOAD,
			editAgency: AGENCY_ACTIONS.EDIT,
			loadAgencies: AGENCY_ACTIONS.LOAD_ALL
		}),
		clearErrorField(field) {
			this.errors.remove(field);
		},
		resetFields() {
			this.formData = {
				_id: null,
				name: null,
				email: null,
				phone: null,
				password: null,
				confirmPassword: null,
				address: null,
				logo: {
					file: null,
					info: {
						size: null,
						type: null,
					}
				},
				status: enumActive.INACTIVE
			};
		},
		resetForm() {
			this.errors.clear();
			$('.preview-img').replaceWith('<div class="preview-img"></div>');
			this.resetFields();
		},
		reload() {
			this.resetFields();
			if (this.$route.params.id) {
				this.loading = true;
				this.loadAgency(this.$route.params.id).then(res => {
					this.parseToFormData(res.agency);
					this.loading = false;
				}).catch(err => {
					console.log('Errors load agencies: ', err);
					this.loading = false;
					return false;
				});
			}
		},
		parseToFormData(data) {
			const {
				_id, name, email, phone, address, status
			} = data;
			this.formData = {
				_id,
				name,
				email,
				phone,
				address,
				status
			};
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		showError(field) {
			return this.errors.has(field) ? 'has-error' : '';
		},
		validateForm() {
			if (!this.isSubmitting) {
				this.$validator.validateAll().then((result) => {
					if (result) {
						this.editAgency(this.formData).then(res => {
							if (res.errors) {
								return this.$errorHandler.showServerErrors(res.errors, this);
							}
							this.isSubmitting = true;
							this.$notify.success('Save agency successfuly');
							return this.$router.push({name: "Agencies-Management"});
						});
					}
				});
			}
		},
	}
};
