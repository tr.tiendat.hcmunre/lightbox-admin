"use strict";
import {mapGetters, mapActions} from 'vuex';
import {BASE_API_URL} from '@/common/configs/api';
import {GETTERS, ACTIONS} from '@/store/const';
import {ACTIONS as AGENCY_ACTIONS} from '@/store/modules/agency/const';

export default {
	data: () => ({
		loaded: true,
		showConfirm: false,
		agencyNeedDeleted: null,
		deleteMsg: ''
	}),
	components: {},
	computed: {
		...mapGetters([
			GETTERS.LOADING
		])
	},
	watch: {
		loading(val) {
			this.loaded = !val;
		},
	},

	methods: {
		...mapActions({
			removeAgency: AGENCY_ACTIONS.REMOVE
		}),
		...mapActions({
			appLoad: ACTIONS.LOAD
		}),
		reload() {
			this.appLoad();
		},
		showNotify(type) {
			this.$notify[type](`${type} message notify`, {
				icon: 'hourglass'
			});
		},
		edit(id) {
			this.$router.push({name: 'Agencies-Edit', params: {id: id}});
		},
		confirmRemove(agency) {
			this.deleteMsg = `Are you sure to delete ${agency.name}?`;
			this.agencyNeedDeleted = agency;
			$("#modal-confirm").modal('show');
		},
		remove() {
			if (!this.agencyNeedDeleted) {
				return;
			}

			this.removeAgency(this.agencyNeedDeleted._id).then(res => {
				// this.$error
				if (res.errors) {
					return this.$errorHandler.showServerErrors(res.errors, this);
				}
				this.table.ajax.reload(null, false); // user paging is not reset on reload
				this.agencyNeedDeleted = null;
				$("#modal-confirm").modal('hide');
				this.$notify.success(`Remove agency successfully`);
			});
		},
		cancel() {
			this.showConfirm = false;
			this.agencyNeedDeleted = null;
		}
	},
	mounted() {
		var that = this;
		this.reload();
		var table = this.table = $(this.$el).find('#agencies').DataTable({
			"serverSide": true,
			"processing": true,
			"pageLength": 10,
			"ajax": $.fn.dataTable.pipeline({
				url: `${BASE_API_URL}/agencies`
			}),
			'info': true,
			'autoWidth': false,
			"columns": [
				{
					"data": 'name',
					"render": function (data, type, row) {
						return data;
					}
				},
				{"data": "email", "width": "20%"},
				{
					"data": "phone",
					"render": function (data, type, row) {
						return data || null;
					}
				},
				{
					"data": "status",
					"width": "80px",
					"render": function (data, type, row) {
						return data.charAt(0).toUpperCase() + data.slice(1);
					}
				},
				{"data": "balance", "width": "80px"},
				{"data": "usage", "width": "80px"},
				{
					"data": null,
					"width": "70px",
					"defaultContent": '<a class="btn btn-info edit"><i class="fa fa-pencil"></i></a> ' +
						'<a  class="btn btn-danger delete"><i class="fa fa-trash"></i></a>'
				}
			],
			"language": {
				"search": "",
				"searchPlaceholder": "Filter",
			},
			"dom": '<"pull-left"f><"col-md-6"iB><"fix-table"t><"clear"><"col-xs-6 col-md-6"l><"col-xs-6 col-md-6"p>'
		});

		$(this.$el).find('#agencies tbody').on('click', 'a', function () {
			var data = table.row($(this).parents('tr')).data();
			if ($(this).hasClass('edit')) {
				that.edit(data._id);
			} else if ($(this).hasClass('delete')) {
				that.confirmRemove(data);
			}
		});
		this.table = table;
	},
	updated() {
		this.$events.$emit(this.$const.SET_PAGE_TITLE, this.$const.AGENCIES_MANAGEMENT);
	},
};
