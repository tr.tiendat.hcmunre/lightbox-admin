'use strict';

const envMap = {
    production: 'prod',
    development: 'dev',
    testing: 'test'
};

console.log(" process.env.NODE_ENV", process.env.NODE_ENV);
const env = process.env.NODE_ENV || 'development';

const isDebug = env === 'development' || env === 'testing';

isDebug && console.log("Start project with environment :", env);

class Config {
    constructor() {
        let config = require(`./${envMap[env]}`) || {};

        this.config = config;
        this.isDebug = isDebug;
    }

    get(key) {
        return this.config[key] || '';
    }
};

export default new Config();
