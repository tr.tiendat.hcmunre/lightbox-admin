'use strict';
const merge = require('webpack-merge');
const prodEnv = require('./prod');

module.exports = merge(prodEnv, {
    apiUrl: 'http://localhost:8041',
});
