import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import $http from 'axios';
import VueAxios from 'vue-axios';
import VeeValidate from 'vee-validate';
import events from './events';
import constants from '@/vuex/constants';
import Notify from './notify';

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueAxios, $http);
Vue.use(VeeValidate);

Vue.events = Vue.prototype.$events = events;
Vue.const = Vue.prototype.$const = constants;
Vue.notify = Vue.prototype.$notify = Notify;
// Vue.$http = Vue.prototype.$http = $http;

$http.interceptors.request.use((request) => {
    const method = request.method.toUpperCase();
    if (method === 'PUT' || method === 'DELETE' || method === 'PATCH') {
        request.headers['X-HTTP-Method-Override'] = method;
        request.method = 'post';
    }
    return request;
});

Vue.mixin({
    methods: {
        hasSlot(name = 'default') {
            return this.$slots[name] && this.$slots[name].length;
        },

        uid(id) {
            return `${this._uid}_${id}`; // eslint-disable-line no-underscore-dangle
        },

        noop() {
            // noop for stopping propagation
        },
    },

    created() {
        this.$main = this.$root.$children[0];
    },
});
