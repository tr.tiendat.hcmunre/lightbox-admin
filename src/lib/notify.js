// register the plugin on vue
import Toasted from 'vue-toasted';
import Vue from 'vue';
import 'assets/scss/notify.scss';

Vue.use(Toasted, {theme: 'bubble', iconPack: 'fontawesome', duration: 5000});

class Notify {
    show(text = '', options = {}) {
        options = Object.assign(options, {
            action: {
                // text: 'close',
                icon: 'times',
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                }
            },
        });

        Vue.toasted.show(text, options).goAway(1500);
    }

    success(text = '', options = {}) {
        options = Object.assign(options, {
            action: {
                icon: 'times',
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                }
            },
        });

        Vue.toasted.success(text, options).goAway(1500);
    }

    info(text = '', options = {}) {
        options = Object.assign(options, {
            action: {
                icon: 'times',
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                }
            },
        });
        Vue.toasted.info(text, options).goAway(1500);
    }
    error(text = '', options = {}) {
        options = Object.assign(options, {
            action: {
                icon: 'times',
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                }
            },
        });
        Vue.toasted.error(text, options);
    }
    clear() {
        Vue.toasted.clear();
    }

};

export default new Notify(Toasted);
