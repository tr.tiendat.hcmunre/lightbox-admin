// js files

// import 'va/lib/script';

// eslint-disable-next-line import/no-webpack-loader-syntax
import 'expose-loader?$!expose-loader?jQuery!jquery';
import 'bootstrap';

import 'jquery-slimscroll';
import 'jquery-ui/ui/widgets/sortable.js';
import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/themes/base/datepicker.css';

import 'admin-lte';
import 'chart.js';

// import 'bootstrap3-wysihtml5-bower/dist/amd/bootstrap3-wysihtml5.all.js'
import 'assets/js/jquery.alterclass.js';
