import Vue from 'vue';
import Row from 'components/Grid/Row';
import Column from 'components/Grid/Column';

Vue.component('row', Row);
Vue.component('column', Column);
