"use strict";

import Select from '../Form/Select/Select';

function formatItems(item) {
	if (item.loading) {
		return item.text;
	}
	var markup = "<div class='select2-result-repository clearfix'>" +
		"<div class='select2-result-item__avatar'><img src='" + require('@/assets/images/marker-icon.png') + "' /></div>" +
		"<div class='select2-result-item__meta'>" +
		"<div class='select2-result-item__title'>" + item.properties['display_name'] + "</div>";

	if (item.description) {
		markup += "<div class='select2-result-item__description'>" + item.description + "</div>";
	}

	markup += "<div class='select2-result-item__statistics'>" +
		"</div>" +
		"</div></div>";

	return markup;
}

function formatItemSelection(item) {
	return item.properties ? item.properties['display_name'] : item.text;
}

export default {
	name: 'SelectLocation',
	props: ['value', 'type'],
	components: {
		Select
	},
	data: () => ({
		options: [],
		defaultUrl: "https://nominatim.openstreetmap.org/search?format=geojson",
		settings: {
			ajax: {
				url: "https://nominatim.openstreetmap.org/search?format=geojson",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
						page: params.page
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					const features = data.features.map((feature, id) => {
						delete feature.type;
						return {
							...feature,
							id,
						};
					});
					return {
						results: features,
						pagination: {
							more: (params.page * 30) < data.features.length
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			placeholder: 'Search for a location',
			minimumInputLength: 1,
			templateResult: formatItems,
			templateSelection: formatItemSelection,
		},
	}),
	method: {
		onChange(evt) {
			this.$emit('changed', evt);
		}
	},
	mounted() {
		let url = `${this.defaultUrl}${this.type !== 'point' ? '&polygon_geojson=1' : ''}`;
		this.settings.ajax.url = url;
		let that = this;

		$(this.$el)
			.select2({
				...this.settings
			}).trigger('change')
			.on('select2:select', function (e) {
				that.$emit('input', this.value);
				that.$emit('changed', { ...e.params, value: this.value });
			});
	},
	watch: {
		value: function (value) {
			// update value
			$(this.$el)
				.val(value)
				.trigger('change');
		},
		type: function (type) {
			// update select
			let url = `${this.defaultUrl}${type !== 'point' ? '&polygon_geojson=1' : ''}`;
			this.settings.ajax.url = url;
			$(this.$el).empty().select2({
				data: this.options,
				...this.settings
			});
		}
	},
	destroyed() {
		$(this.$el).off().select2('destroy');
	},
	updated() {
	}
};
