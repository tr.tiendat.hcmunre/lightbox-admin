import Vue from 'vue';
import Row from './Grid/Row';
import Column from './Grid/Column';

Vue.component('row', Row);
Vue.component('column', Column);
