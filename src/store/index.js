import Vue from 'vue';
import Vuex, { Store } from "vuex";
import { state } from './state';
import { actions } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";
import modules from "./modules";

Vue.use(Vuex);

export * from './const';

const store = new Store({
	state,
	getters, // global getters
	actions, // global actions
	mutations, // global mutations
	modules,
	strict: process.env.NODE_ENV !== "production"
});

export default store;
