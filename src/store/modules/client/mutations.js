import _ from 'lodash';
import CLIENT from './const';

export const mutations = {
	[CLIENT._MUTATES.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};
