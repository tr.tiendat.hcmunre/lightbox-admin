import CLIENT from "./const";

export const getters = {
	[CLIENT._GETTERS.USERS]: state => state.users,
};
