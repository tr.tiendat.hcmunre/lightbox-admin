import MESSAGES from './const';

export const actions = {
	[MESSAGES._ACTIONS.ADD_MESSAGE]: ({ commit }, message) => {
		commit(MESSAGES._MUTATES.ADD_MESSAGE, message);
	}
};
