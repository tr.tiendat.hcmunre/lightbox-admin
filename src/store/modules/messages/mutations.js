import MESSAGES from "./const";

export const mutations = {
	[MESSAGES._MUTATES.ADD_MESSAGE]: (state, message) => {
		state.messages.push(message);
	}
};
