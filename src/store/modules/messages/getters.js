import MESSAGES from "./const";

export const getters = {
	[MESSAGES._GETTERS.MESSAGES]: state => state.messages,
};
