export const MODULE_NAME = 'messages';

export const _GETTERS = {
	MESSAGES: 'messages'
};

export const GETTERS = {
	MESSAGES: `${MODULE_NAME}/${_GETTERS.MESSAGES}`
};

export const _ACTIONS = {
	ADD_MESSAGE: 'addMessage',
};

export const ACTIONS = {
	ADD_MESSAGE: `${MODULE_NAME}/${_ACTIONS.ADD_MESSAGE}`,
};

export const _MUTATES = {
	ADD_MESSAGE: 'addMessage',
};

export const MUTATES = {
	ADD_MESSAGE: `${MODULE_NAME}/${_MUTATES.ADD_MESSAGE}`,
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATES
};
