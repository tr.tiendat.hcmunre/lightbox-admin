import * as adSettingApi from '@/services/ad.service';
import AD_SETTING from './const';

export const actions = {
	[AD_SETTING._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return adSettingApi.loadAdSetting().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, attributes: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};
