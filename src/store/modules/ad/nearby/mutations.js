import _ from 'lodash';
import NEARBY from './const';

export const mutations = {
	[NEARBY._MUTATORS.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};
