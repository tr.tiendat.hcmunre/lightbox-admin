import * as nearbyApi from '@/services/ad.nearby.service';
import NEARBY from './const';

export const actions = {
	[NEARBY._ACTIONS.LOAD]: ({ commit }, id) => {
		return nearbyApi.loadNearby(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, nearby: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[NEARBY._ACTIONS.REMOVE]: ({ commit }, id) => {
		return nearbyApi.removeNearby(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[NEARBY._ACTIONS.EDIT]: ({ commit }, nearby) => {
		let req = location._id ? nearbyApi.updateNearby(nearby._id, nearby) : nearbyApi.addNearby(nearby);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, location: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[NEARBY._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return nearbyApi.loadNearbys().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, locations: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};
