import _ from 'lodash';
import AD from './const';

export const mutations = {
	[AD._MUTATORS.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};
