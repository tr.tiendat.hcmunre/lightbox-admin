import * as timeApi from '@/services/ad.time.service';
import TIME from './const';

export const actions = {
	[TIME._ACTIONS.LOAD]: ({ commit }, id) => {
		return timeApi.loadTime(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, location: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[TIME._ACTIONS.REMOVE]: ({ commit }, id) => {
		return timeApi.removeTime(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[TIME._ACTIONS.EDIT]: ({ commit }, time) => {
		let req = location._id ? timeApi.updateTime(location._id, location) : timeApi.addTime(location);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, location: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[TIME._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return timeApi.loadTimes().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, locations: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};
