import * as adApi from '@/services/ad.location.service';
import LOCATION from './const';

export const actions = {
	[LOCATION._ACTIONS.LOAD]: ({ commit }, id) => {
		return adApi.loadLocation(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, location: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[LOCATION._ACTIONS.REMOVE]: ({ commit }, id) => {
		return adApi.removeLocation(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[LOCATION._ACTIONS.EDIT]: ({ commit }, location) => {
		let req = location._id ? adApi.updateLocation(location._id, location) : adApi.addLocation(location);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, location: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[LOCATION._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return adApi.loadLocations().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, locations: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};
