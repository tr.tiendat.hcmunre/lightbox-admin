import _ from 'lodash';
import LOCATION from './const';

export const mutations = {
	[LOCATION._MUTATORS.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};
