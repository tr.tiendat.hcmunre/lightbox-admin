import NOTIFICATIONS from './const';

export const actions = {
	[NOTIFICATIONS._ACTIONS.ADD_NOTIFY]: ({ commit }, notify) => {
		commit(NOTIFICATIONS._MUTATES.ADD_NOTIFY, notify);
	}
};
