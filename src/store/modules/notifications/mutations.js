import NOTIFICATIONS from "./const";

export const mutations = {
	[NOTIFICATIONS._MUTATES.ADD_NOTIFY]: (state, notify) => {
		state.notifies.push(notify);
	},
};
