import NOTIFICATIONS from "./const";

export const getters = {
	[NOTIFICATIONS._GETTERS.NOTIFIES]: state => state.notifies
};
