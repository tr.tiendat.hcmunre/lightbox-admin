import SETTINGS from './const';

export const actions = {
	[SETTINGS._ACTIONS.SET_SKIN]: ({commit}, skin) => {
		commit(SETTINGS._MUTATES.SET_SKIN, skin);
	}
};
