import SETTINGS from "./const";

export const mutations = {
	[SETTINGS._MUTATES.SET_SKIN]: (state, skin) => {
		state.settings.skin = skin;
	}
};
