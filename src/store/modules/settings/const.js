export const MODULE_NAME = 'settings';

const _GETTERS = {
	SETTINGS: 'settings'
};

export const GETTERS = {
	SETTINGS: `${MODULE_NAME}/${_GETTERS.SETTINGS}`
};

const _ACTIONS = {
	SET_SKIN: 'setSkin'
};

export const ACTIONS = {
	SET_SKIN: `${MODULE_NAME}/${_ACTIONS.SET_SKIN}`
};

const _MUTATES = {
	SET_SKIN: 'setSkin'
};

export const MUTATES = {
	SET_SKIN: `${MODULE_NAME}/${_MUTATES.SET_SKIN}`
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATES
};
