import { ACTIONS, MUTATES } from "./const";

export const actions = {
	[ACTIONS.SET_USER]: ({commit}, user) => {
		commit(MUTATES.SET_USER, user);
	}
};
