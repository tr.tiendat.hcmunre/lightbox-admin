import _ from 'lodash';
import { MUTATES } from './const';

const moduleName = 'user';

export const USER_MUTATES = {
	SET_AUTH: `${moduleName}/${MUTATES.SET_AUTH}`,
	SET_PROFILE: `${moduleName}/${MUTATES.SET_PROFILE}`,
};

export const mutations = {
	[USER_MUTATES.SET_USER](state, users) {
		state.users = _.uniqBy(state.users, users, '_id');
	},

	[MUTATES.SET_AUTH]: (state, authenticated) => {
		state.isAuth = !!authenticated;
	},

	[MUTATES.SET_PROFILE]: (state, info) => {
		state.profile = info || {};
	},
};
