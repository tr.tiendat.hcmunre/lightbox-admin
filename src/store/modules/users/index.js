import { state } from './state';
import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';

export const MODULE_NAME = 'user';

export { USER_MUTATES } from './mutations';

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations
};
