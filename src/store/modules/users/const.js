export const MODULE_NAME = 'users';

const _GETTERS = {
	USERS: 'users'
};

export const GETTERS = {
	USERS: `${MODULE_NAME}/${_GETTERS.USERS}`
};

const _ACTIONS = {
	SET_USER: 'setUsers'
};

export const ACTIONS = {
	SET_USER: `${MODULE_NAME}/${_ACTIONS.SET_USER}`
};

const _MUTATES = {
	SET_USER: 'setUsers',
	SET_AUTH: 'setAuth',
	SET_PROFILE: 'setProfile',
};

export const MUTATES = {
	SET_USER: `${MODULE_NAME}/${_MUTATES.SET_USER}`,
	SET_AUTH: `${MODULE_NAME}/${_MUTATES.SET_USER}`,
	SET_PROFILE: `${MODULE_NAME}/${_MUTATES.SET_PROFILE}`,
};
