import _ from 'lodash';
import AGENCY from './const';

export const mutations = {
	[AGENCY._MUTATORS.SET_USERS]: (state, users) => {
		state.users = _.uniqBy(state.users, users, '_id');
	}
};
