import AGENCY from './const';

export const getters = {
	[AGENCY._GETTERS.USERS]: state => state.users,
};
