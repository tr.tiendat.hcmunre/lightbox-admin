import * as agencyApi from '../../../services/agency.service';
import AGENCY from './const';

export const actions = {
	[AGENCY._ACTIONS.LOAD]: ({ commit }, id) => {
		return agencyApi.loadAgency(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, agency: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AGENCY._ACTIONS.REMOVE]: ({ commit }, id) => {
		return agencyApi.removeAgency(id).then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AGENCY._ACTIONS.EDIT]: ({ commit }, agency) => {
		let req = agency._id ? agencyApi.updateAgency(agency._id, agency) : agencyApi.addAgency(agency);
		return req.then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, client: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	},

	[AGENCY._ACTIONS.LOAD_ALL]: ({ commit }) => {
		return agencyApi.loadAgencies().then(res => {
			if (res.error) {
				return { errors: res.errors || res.error };
			}
			return { success: true, agencies: res.data };
		}).catch(res => {
			return { errors: res.error };
		});
	}
};
