const MODULE_NAME = 'agency';

const _GETTERS = {
	USERS: 'users'
};

export const GETTERS = {
	USERS: `${MODULE_NAME}/${_GETTERS.USERS}`
};

const _ACTIONS = {
	LOAD: 'load',
	REMOVE: 'remove',
	EDIT: 'edit',
	LOAD_ALL: 'loadAll'
};

export const ACTIONS = {
	LOAD: `${MODULE_NAME}/${_ACTIONS.LOAD}`,
	REMOVE: `${MODULE_NAME}/${_ACTIONS.REMOVE}`,
	EDIT: `${MODULE_NAME}/${_ACTIONS.EDIT}`,
	LOAD_ALL: `${MODULE_NAME}/${_ACTIONS.LOAD_ALL}`
};

const _MUTATORS = {
	SET_USERS: 'setUsers'
};

export const MUTATORS = {
	SET_USERS: `${MODULE_NAME}/${_MUTATORS.SET_USERS}`
};

export default {
	_GETTERS,
	_ACTIONS,
	_MUTATORS
};
