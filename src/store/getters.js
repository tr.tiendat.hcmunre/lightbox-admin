import { GETTERS } from './const';
import { BASE_API_URL } from '../common/configs/api';

export const getters = {
	[GETTERS.UNREAD_MSG_COUNT]: (state) => state.messages.messages.length,
	[GETTERS.UNREAD_NOTICE_COUNT]: (state) => state.notifications.notifies.length,
	[GETTERS.CURRENT_USER]: (state) => state.currentUser,
	[GETTERS.AVATAR]: (state) => `${BASE_API_URL}/user/${state.currentUser._id}/avatar/${state.currentUser.picture}?_=${Date.now()}`,
	[GETTERS.SKIN]: (state) => state.settings.skin,
	[GETTERS.IS_AUTHENTICATED]: (state) => !!state.currentUser._id,
	[GETTERS.LOADING]: (state) => {
		return state.loading;
	},
	[GETTERS.SAVING]: (state) => state.saving
};
