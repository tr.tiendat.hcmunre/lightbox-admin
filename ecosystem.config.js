module.exports = {
    apps: [
        {
            name: "lightbox-admin",
            script: "./server",
            watch: true,
            env: {
                // "PORT": 8042,
                "NODE_ENV": "staging"
            },
            env_production: {
                // "PORT": 8042,
                "NODE_ENV": "production",
            },
            env_testing: {
                // "PORT": 8042,
                "NODE_ENV": "testing",
            }
        }
    ]
};
