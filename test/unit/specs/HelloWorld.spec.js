import Vue from 'vue';
import About from '@/pages/About';

describe('Home.vue', () => {
	it('should render correct contents', () => {
		const Constructor = Vue.extend(About);
		const vm = new Constructor().$mount();
		expect(vm.$el.querySelector('a.link').textContent)
			.toEqual('test link');
	});
});
